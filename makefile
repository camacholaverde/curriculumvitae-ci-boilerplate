TEX = pandoc
FLAGS = --latex-engine=xelatex
BUILDDIR = public

src := $(wildcard *.yml)
TARGETS := $(patsubst %.yml,$(BUILDDIR)/%.pdf,$(src))
TEMPLATE := template.tex

clsrc := $(filter-out README.md, $(wildcard *.md))
CLTARGETS := $(patsubst %.md,$(BUILDDIR)/%.pdf,$(clsrc))
CLTEMPLATE := coverlettertemplate.tex

all: $(TARGETS)

$(TARGETS): $(BUILDDIR)/%.pdf : %.yml $(TEMPLATE)
	$(TEX) $< --verbose -o $@ --template=$(TEMPLATE) $(FLAGS)

all: $(CLTARGETS)

$(CLTARGETS): $(BUILDDIR)/%.pdf : %.md $(CLTEMPLATE)
	$(TEX) $< --verbose -o $@ --template=$(CLTEMPLATE) $(FLAGS)

.PHONY: clean
clean :
	rm public/*.pdf
