---
subject: Application for iOS Developer Position
author: Alejandro Camacho
city: Medellin
from:
- Kra 28 \#29-145,
- Apt 1304, Medellin

# Settings
mainfont: Linux Libertine O # fonts: Hoefler Text / OFL Sorts Mill Goudy / Sabon LT Std / Cardo / Linux Libertine O | you can use any good font, put your .otf in the fonts dir
lang: english # do not use capital letters: dutch / german
fontsize: 10pt # 10pt or 11pt or 12pt
customdate: 2020-03-02 #disabled for now
textcolor: "000000" # use hexcolor, 000000 for black
linkcolor: "1EAEDB" # use hexcolor, FF0000 for red
pagecolor: "FFFFFF" # use hexcolor, FFFFFF for white
# letterhead: true
# geometry: a4paper, left=35mm, right=35mm, top=50mm, bottom=25mm
geometry: a4paper, left=35mm, right=35mm, top=51mm, bottom=30mm
geometrysecondarypagestop: 11mm # this will be minus! So 51mm - 21mm
signaturefile: signature.jpeg # file plus extension like: signature.pdf or signature.jpg
signatureheight: 3 # is cm
donothidepersonalinformation: true # If your letterhead includes your personal information put in "false" or comment it out
letterhead: #letterhead.pdf # file plus extension like: letterhead.pdf
letterheadfront: #letterheadfront.pdf # file plus extension like: letterheadfront.pdf - To include a different letterhead on the first page
---

Dear recruiter team at Innovattic,

I recently received an invitation to apply as developer to your company. For me to join a company, it is mandatory that they have among their principles to create great experiences, memorable to clients and to provide an enjoyable, dynamic environment for workers. It seems to me that Innovattic fits within this category. I also believe that the skills and experience that I have may interest you as well.

I started my journey in iOS Development back in 2012, as a personal initiative for developing software for the hardware I came to love since the first time I used it back in 2009. The Apple philosophy, always focused on the user, and aiming to give  an incredible experience combined with a great performance, always thrilled me, and I have been hooked to this way of thinking since day one. I have extensive experience developing iOS apps, for different fields such as medical, teaching, banking and traveling.

I am confident that my experience as iOS Developer, my software development skills combined with my formation in Biomedical Engineering qualify me for consideration. If you would like, I can provide you with current samples of my work. I have also enclosed my resume. I look forward to meeting with you and discussing my qualifications in more detail.

Sincerely,
